package models;


public class Vertex {
    final private String id;


    public Vertex(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj != null && getClass() == obj.getClass()) {
            Vertex other = (Vertex) obj;
            return id != null && (other.id == null || id.equals(other.id));
        }
        return false;
    }

    @Override
    public String toString(){
        return id;
    }
}