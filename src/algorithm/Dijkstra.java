package algorithm;

import models.Edge;
import models.Graph;
import models.Vertex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dijkstra {
    private final List<Edge> edges;
    private Set<Vertex> settledVertex;
    private Set<Vertex> unSettledVertex;
    private Map<Vertex, Vertex> predecessors;
    private Map<Vertex, Integer> distance;

    public Dijkstra(Graph graph) {
        this.edges = new ArrayList<>(graph.GetEdges());
    }

    /**
     * Executes the algorithm.Dijkstra algorithm
     *
     * @param startVertex The vertex to start from.
     */
    public void Execute(Vertex startVertex) {
        settledVertex = new HashSet<>();
        unSettledVertex = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(startVertex, 0);
        unSettledVertex.add(startVertex);
        while (unSettledVertex.size() > 0) {
            Vertex vertex = GetMinimum(unSettledVertex);
            settledVertex.add(vertex);
            unSettledVertex.remove(vertex);
            FindMinimalDistances(vertex);
        }
    }

    private void FindMinimalDistances(Vertex vertex) {
        List<Vertex> adjacentVertex = GetNeighbors(vertex);
        for (Vertex target : adjacentVertex) {
            if (GetShortestDistance(target) > GetShortestDistance(vertex) + GetDistance(vertex, target)) {
                distance.put(target, GetShortestDistance(vertex) + GetDistance(vertex, target));
                predecessors.put(target, vertex);
                unSettledVertex.add(target);
            }
        }
    }

    private int GetDistance(Vertex vertex, Vertex target) {
        for (Edge edge : edges) {
            if (edge.GetSource().equals(vertex) && edge.GetDestination().equals(target)) {
                return edge.GetWeight();
            }
        }
        throw new RuntimeException("Something weird happened with GetDistance?");
    }

    private List<Vertex> GetNeighbors(Vertex vertex) {
        List<Vertex> neighbors = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.GetSource().equals(vertex) && !IsSettled(edge.GetDestination())) {
                neighbors.add(edge.GetDestination());
            }
        }
        return neighbors;
    }

    private Vertex GetMinimum(Set<Vertex> vertices) {
        Vertex minimum = vertices.iterator().next();
        for (Vertex vertex : vertices) {
            if (GetShortestDistance(vertex) < GetShortestDistance(minimum)) {
                minimum = vertex;
            }
        }
        return minimum;
    }

    private boolean IsSettled(Vertex vertex) {
        return settledVertex.contains(vertex);
    }

    private int GetShortestDistance(Vertex destination) {
        Integer distance = this.distance.get(destination);
        return distance == null ? Integer.MAX_VALUE : distance;
    }

    /**
     * Get the finished path
     *
     * @param targetVertex The vertex the path should end at.
     * @return A linked list of the vertices to traverse.
     * @remarks If no valid path is found it returns null.
     */
    public LinkedList<Vertex> GetPath(Vertex targetVertex) {
        LinkedList<Vertex> path = new LinkedList<>();
        Vertex step = targetVertex;
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        Collections.reverse(path);
        return path;
    }
}
