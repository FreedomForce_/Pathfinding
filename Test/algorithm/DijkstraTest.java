package algorithm;

import models.Graph;
import models.Vertex;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


class DijkstraTest {

    @Test
    void TestExecute01() {
        Graph graph = new Graph();
        graph.AddVertices(11);
        graph.AddEdge("Edge_0", 0, 1, 85);
        graph.AddEdge("Edge_1", 0, 2, 217);
        graph.AddEdge("Edge_2", 0, 4, 173);
        graph.AddEdge("Edge_3", 2, 6, 186);
        graph.AddEdge("Edge_4", 2, 7, 103);
        graph.AddEdge("Edge_5", 3, 7, 183);
        graph.AddEdge("Edge_6", 5, 8, 250);
        graph.AddEdge("Edge_7", 8, 9, 84);
        graph.AddEdge("Edge_8", 7, 9, 167);
        graph.AddEdge("Edge_9", 4, 9, 502);
        graph.AddEdge("Edge_10", 9, 10, 40);
        graph.AddEdge("Edge_11", 1, 10, 600);

        Dijkstra dijkstra = new Dijkstra(graph);
        dijkstra.Execute(graph.GetVertexes().get(0));
        LinkedList<Vertex> path = dijkstra.GetPath(graph.GetVertexes().get(10));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        LinkedList<Vertex> testPath = new LinkedList<>();
        testPath.add(new Vertex("Vertex_0"));
        testPath.add(new Vertex("Vertex_2"));
        testPath.add(new Vertex("Vertex_7"));
        testPath.add(new Vertex("Vertex_9"));
        testPath.add(new Vertex("Vertex_8"));
        assert (path.containsAll(testPath));
    }


    @Test
    void testExecute02() {
        Graph graph = new Graph();
        graph.AddVertices(8);

        graph.AddEdge("Edge_0", 1, 2, 85);
        graph.AddEdge("Edge_1", 1, 3, 217);
        graph.AddEdge("Edge_2", 1, 4, 173);
        graph.AddEdge("Edge_3", 2, 7, 186);
        graph.AddEdge("Edge_4", 2, 3, 103);
        graph.AddEdge("Edge_5", 3, 4, 250);
        graph.AddEdge("Edge_6", 4, 5, 84);
        graph.AddEdge("Edge_7", 5, 6, 167);
        graph.AddEdge("Edge_8", 6, 7, 502);

        Dijkstra dijkstra = new Dijkstra(graph);
        dijkstra.Execute(graph.GetVertexes().get(1));
        LinkedList<Vertex> path = dijkstra.GetPath(graph.GetVertexes().get(6));

        assertNotNull(path);
        assertTrue(path.size() > 0);
        LinkedList<Vertex> testPath = new LinkedList<>();
        testPath.add(new Vertex("Vertex_1"));
        testPath.add(new Vertex("Vertex_4"));
        testPath.add(new Vertex("Vertex_5"));
        testPath.add(new Vertex("Vertex_6"));
        assert (path.containsAll(testPath));
    }
}
